const path = require('path')
const mergeIntoSingleFilePlugin = require('webpack-merge-and-include-globally')
const uglifyJs = require('uglify-js')
const srcPath = (subdir) => {
  return path.join(__dirname, 'src', subdir)
}

const mergeVendor = new mergeIntoSingleFilePlugin({
      files: {
          'vendor.js': [
              'bin/libs/laya.core.js',
              'bin/libs/laya.wxmini.js',
              'bin/libs/laya.bdmini.js',
              'bin/libs/laya.webgl.js',
              'bin/libs/laya.ani.js',
              'bin/libs/laya.filter.js',
              'bin/libs/laya.html.js',
              'bin/libs/laya.particle.js',
              'bin/libs/laya.tiledmap.js',
              'bin/libs/laya.ui.js',
              'bin/libs/fairygui/fairygui.min.js',
	            'bin/libs/rawinflate/rawinflate.min.js'
          ]
      },
      transform: {
        'vendor.js': code => uglifyJs.minify(code).code
      }
})

module.exports = {
  devServer: {
    host: 'localhost',
    port: '3000',
    hot: true,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    historyApiFallback: true,
  },
  entry: path.join(__dirname, 'src/main.ts'),
  output: {
    filename: 'ui.bundle.min.js',
    path: path.join(__dirname, 'bin/js'),
  },
  
  resolve: {
    extensions: ['.ts', '.js', '.json'],
    alias: {
      ui: srcPath('ui'),
      language: srcPath('language'),
    },
  },
  //plugins: [mergeVendor],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['es2015']
          }
        },
      }
    ]
  }
}