const Loader = Laya.Loader
const Handler = Laya.Handler
import MainScreen from './ui/screen/main'

export default class Main {
    constructor(){
       this.loadStage()
    }

    loadStage(): void{
        Laya.init ( 640 , 480 , Laya.WebGL)
        laya.utils.Stat.show(0 , 0)
        Laya.stage.scaleMode = 'showall'
        Laya.stage.alignH = 'left'
        Laya.stage.alignV = 'top'
        Laya.stage.screenMode = 'horizontal'

        Laya.loader.load([{
            url: 'res/Package1_atlas0.png',
            type: Loader.IMAGE
        },
        { 
            url: 'res/Package1.fui', 
            type: Loader.BUFFER 
        }], Handler.create(this, this.onLoaded))
    }
   
    onLoaded(): void {
        Laya.stage.addChild(fairygui.GRoot.inst.displayObject)
        fairygui.UIPackage.addPackage('res/Package1')
        fairygui.UIConfig.defaultFont = 'Gabriola'
        new MainScreen()
    }
}

new Main()