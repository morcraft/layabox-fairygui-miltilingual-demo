type Exactly<T, U> = { [K in keyof U]: K extends keyof T ? T[K] : never };//check whether property signatures match
//TODO: improve definition to support bidirectional checking
export default Exactly