import UIDefinitions from '../ui/definitions'
import Exactly from 'language/exactly'

export default class VietnameseDictionary implements UIDefinitions, Exactly<UIDefinitions, VietnameseDictionary>{
    MAIN_TEXT_1 = "Văn bản đầu tiên"
    MAIN_TEXT_2 = "Văn bản thứ hai"
    MAIN_TEXT_3 = "Văn bản thứ ba"
    MAIN_TEXT_4 = "Một số văn bản tiếng việt"
    RICH_TEXT_1 = "Một số văn bản phong phú"
    RICH_TEXT_2 = "Một số văn bản phong phú khác"
}