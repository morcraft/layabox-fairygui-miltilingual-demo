import UIDefinitions from '../ui/definitions'
import Exactly from 'language/exactly'

export default class ChineseDictionary implements UIDefinitions, Exactly<UIDefinitions, ChineseDictionary>{
    MAIN_TEXT_1 = "第一个文字"
    MAIN_TEXT_2 = "第二个文字"
    MAIN_TEXT_3 = "第三文"
    MAIN_TEXT_4 = "第四个案文"
    RICH_TEXT_1 = "丰富的文字"
    RICH_TEXT_2 = "另一个富文本"
}