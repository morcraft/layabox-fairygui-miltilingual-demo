import UIDefinitions from '../ui/definitions'
import Exactly from 'language/exactly'

export default class EnglishDictionary implements UIDefinitions, Exactly<UIDefinitions, EnglishDictionary>{
    MAIN_TEXT_1 = "asdasdasdas"
    MAIN_TEXT_2 = "Second text"
    MAIN_TEXT_3 = "Third text"
    MAIN_TEXT_4 = "Fourth text"
    RICH_TEXT_1 = "A rich text"
    RICH_TEXT_2 = "Another rich text"
}