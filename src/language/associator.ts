import UIDictionary from './ui/definitions'
import UIMap from './ui/elementsMap';

export default class LanguageAssociator{
    public static associateView(view: fairygui.GComponent, dictionary: UIDictionary){
        for(let elementId in UIMap){
            const child = view.getChild(elementId)
            if(child === Object(child)){//element exists
                const dictionaryIndex = UIMap[elementId]
                child.text = dictionary[dictionaryIndex]//update text
            }
        }
    }
}