import UIDefinitions from './definitions'

interface Dictionary{
    [s: string]: keyof UIDefinitions
}

const uiMap: Dictionary = {
    first_text: 'MAIN_TEXT_1',
    second_text: 'MAIN_TEXT_2',
    third_text: 'MAIN_TEXT_3',
    fourth_text: 'MAIN_TEXT_4',
    rich_text_1: 'RICH_TEXT_1',
    rich_text_2: 'RICH_TEXT_2'
} 

export default uiMap