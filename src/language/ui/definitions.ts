interface UIDefinitions{
    MAIN_TEXT_1: string,
    MAIN_TEXT_2: string,
    MAIN_TEXT_3: string,
    MAIN_TEXT_4: string,
    RICH_TEXT_1: string,
    RICH_TEXT_2: string
}

export default UIDefinitions