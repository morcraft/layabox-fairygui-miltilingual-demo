import TransitionPlayer from '../transition/player'
import ProgressBarHandler from '../progressBar/handler'
import ButtonsHandler from '../button/handler'

export default class MainScreen {
    private ProgressBarHandler: ProgressBarHandler
    private TransitionPlayer: TransitionPlayer
    private ButtonsHandler: ButtonsHandler
    private view: fairygui.GComponent

    public constructor() {
        window['checkMeDawg'] = this
        this.view = fairygui.UIPackage.createObject('Game', 'Main').asCom
        this.view.setSize(fairygui.GRoot.inst.width, fairygui.GRoot.inst.height)
        fairygui.GRoot.inst.addChild(this.view)

        this.TransitionPlayer = new TransitionPlayer(this.view, 3)
        this.TransitionPlayer.playTransition(0)

        this.ProgressBarHandler = new ProgressBarHandler(this.view.getChild('CoolLoader').asProgress)
        this.ProgressBarHandler.simulateProgress()

        this.ButtonsHandler = new ButtonsHandler(this.view, true)
    }
}
