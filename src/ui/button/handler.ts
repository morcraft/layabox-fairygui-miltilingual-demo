import LanguageAssociator from 'language/associator'
import ChineseDictionary from 'language/dictionary/chinese'
import EnglishDictionary from 'language/dictionary/english'
import VietnameseDictionary from 'language/dictionary/vietnamese'
import UIDictionary from 'language/ui/definitions';

export default class ButtonsHandler{
    private dictionaries = {
        'Chinese': ChineseDictionary,
        'English': EnglishDictionary,
        'Vietnamese': VietnameseDictionary
    }

    private languages = {}

    constructor(readonly view: fairygui.GComponent, autoStart?: boolean){
        if(autoStart === true) this.autoStart()
    }

    private autoStart(): void{
        LanguageAssociator.associateView(this.view, new ChineseDictionary())

        for(const language in this.dictionaries){
            const button: fairygui.GButton = this.view.getChild(language + 'LangBtn').asButton
            if(button === null) throw new Error('Invalid language button ' + language)
            this.languages[language] = new this.dictionaries[language]
            this.setUpLanguageButton(button, this.languages[language])
        }
    }

    public setUpLanguageButton(button: fairygui.GButton, language: UIDictionary): fairygui.GButton{
        button.onClick(this, () => LanguageAssociator.associateView(this.view, language))
        return button
    }
}