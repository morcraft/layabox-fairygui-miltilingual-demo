export default class TransitionPlayer{
    private _view: fairygui.GComponent
    private _transitionNumber: number = 0
    private _transitionRange: number
    private _transitionId: fairygui.Transition

    public constructor(view: fairygui.GComponent, range: number){
        this._view = view
        this._transitionRange = range
        this._transitionId = this._view.getTransition('t0')
    }

    playTransition(transitionNumber: number): void{
        if(transitionNumber > this._transitionRange) transitionNumber = 0
        this._transitionNumber = transitionNumber
        this._transitionId = this._view.getTransition('t' + transitionNumber)
        return this._transitionId.play(Laya.Handler.create(this, this.onTransitionEnd))
    }

    onTransitionEnd(): void{
        return this.playTransition(this._transitionNumber + 1)
    }
}