export default class ProgressBarHandler{
    private _progressBar: fairygui.GProgressBar

    public constructor(progressBar: fairygui.GProgressBar){
        this._progressBar = progressBar
    }

    setValue(value: number){
        if(value < 0) throw new Error('Passed value is less than zero')
        if(value > this._progressBar.max) throw new Error('Passed value is greater than max value in ProgressBar')
        this._progressBar.value = value
    }

    increaseCurrentValue(): void{
        let value = this._progressBar.value + 1
        if(value > this._progressBar.max) value = 0
        this.setValue(value)
    }

    startIncreaseTimer(){
        return Laya.timer.frameLoop(2, this, this.increaseCurrentValue)
    }

    stopIncreaseTimer(){
        return Laya.timer.clear(this, this.increaseCurrentValue)
    }

    simulateProgress(): void
    simulateProgress(stop: boolean, after: number): void
    simulateProgress(stop?: boolean, after?: number): void{
        this.startIncreaseTimer()
        if(stop === true){
            Laya.timer.once(after, this, this.stopIncreaseTimer)
        }
    }
}